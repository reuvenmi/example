<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public $role;
	public $category;
    
    public static function tableName(){
        return 'user';// חיבור לטבלת יוזר 
    }
   
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)// החזרת הת.ז של המשתמש במסד נתוים
    {
        return self::findOne($id); // בגלל הקישור של הטבלה ליוזר בפונקציה הקודמת נדע לאן ללכת
    }
    
    public function rules() // כללים עבור השדות בטבלת יוזר
    {
        return
        [
            [['username', 'password', 'authkey'],'string', 'max' =>255],
            [['categoryid'],'integer'],
            [['username', 'password'],'required'],
            [['username'], 'unique'],
            ['role', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)// ?
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)// הפונקציה תקבל שם משתמש ותחזיר את האובייקט המתייחס לשם זה
    {
        return self::findOne(['username'=>$username]);// בתוך הסוגריים נכתוב את התנאי שדרכו נמצא את האובייקט הרלוונטי
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->authkey === $authkey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
  
  
  //test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
    
    private function isCorrectHash($plaintext, $hash)
    {
        return Yii::$app->security->validatePassword($plaintext, $hash);
    }
  
  
  
  
  
    //hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
                    generatePasswordHash($this->password);

        if ($this->isNewRecord)
            $this->authkey = Yii::$app->security->generateRandomString(32);

        return $return;
    }
    
    
   
   
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRoles($roleName);
		if (\Yii::$app->authManager->getRoles($this->id) == null){
			$auth->assign($role, $this->id);
		} 
		else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }	
	
    
   

    //create fullname pseuodo field תכונה מדומה -
    public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

    
    
    
//A method to get an array of all users models/User
    public static function getUsers()
    {
        $users = ArrayHelper::
                    map(self::find()->all(), 'id', 'fullname');// מערך של מערכים עם שם ות.ז לכל היוזרים
        return $users;                      
    }

    
    
//A method to get an array of all roles
    
    public static function getRoles()
    {

        $rolesObjects = Yii::$app->authManager->getRoles();
        $roles = [];
        foreach($rolesObjects as $id =>$rolObj){
            $roles[$id] = $rolObj->name; 
        }
        
        return $roles;      
    }   

   
    
}
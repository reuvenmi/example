<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activity;

/**
 * Activitysearch represents the model behind the search form about `app\models\Activity`.
 */
class Activitysearch extends Activity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'categoryid', 'statusid'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categoryid' => $this->categoryid,
            'statusid' => $this->statusid,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
		
		// grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categoryid' => $this->categoryid,
            'statusid' => $this->statusid,
        ]);
		
		//to guest user, show only 'published (id=1)' status
		if(Yii::$app->user->isGuest){
			$query->andFilterWhere(['=','statusid','1']);
		}
		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like','id', $this->id])
			->andFilterWhere(['like','statusid', $this->statusid]);
			
		return $dataProvider;
    }

     
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryid
 * @property integer $statusid
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryid', 'statusid'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 //יעכגעג
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryid' => 'Categoryid',
            'statusid' => 'Statusid',
        ];
    }
	//עזר לשינוי קטגורי
		public function getFindCategory()
    {
		return  Category::findOne($this->categoryid);
    }
	
	public function getFindStatus()
    {
		return  status::findOne($this->statusid);
    }
	
}

<?php
	
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\status;
use app\models\Category;


/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryid')->dropDownList(category::getCategories());  ?>

    <!--?= $form->field($model, 'statusId')->dropDownList(Status::getStatuses()/*,['disabled'=>true]*/); ?--> 
	<!--if it's new record (create)-> it's hide the under form and send '2' to the datebase, else (update) -> its show option of statusId-->
	<? if($model->isNewRecord){ ?>
			<div style="display:none;"> <?= $form->field($model, 'statusId')->textInput(['value'=>2]) ?> </div>
		<? }else{ ?>
			<?= $form->field($model, 'statusid')-> dropDownList(Status::getStatuses()) ?>
	<? } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	
	
	

</div>

<?php

use yii\db\Migration;

/**
 * Handles adding rool to table `user`.
 */
class m170718_135539_add_rool_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'role', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'role');
    }
}

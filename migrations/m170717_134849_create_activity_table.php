<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170717_134849_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'categoryid' => $this->integer(),
            'statusid' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}

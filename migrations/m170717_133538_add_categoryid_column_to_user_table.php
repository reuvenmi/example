<?php

use yii\db\Migration;

/**
 * Handles adding categoryid to table `user`.
 */
class m170717_133538_add_categoryid_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'categoryid', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'categoryid');
    }
}
